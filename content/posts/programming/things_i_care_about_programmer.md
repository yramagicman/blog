---
title: "Things I Care About As a Programmer"
date: 2020-11-19T17:57:05-05:00
draft: false
---


## 1. My editor

Like all programmers, I spend 95% of my time in my editor. As a result, my editor the tool I care about most, but interestingly enough, I wouldn't call it the most important tool. Personally, I'm a fan of Vim, however I've spent a fair amount of time in EMACS as well. In the end what matters to me is that the editor doesn't get in my way, and right now Vim provides the shortest path from my brain to the screen. EMACS with evil-mode is a very close second, and would likely be in first if I spent more time on it. However, I don't have the motivation to take that kind of time to make EMACS work given that I already have a wonderfully viable solution.

## 2. Workflow

This is probably the most important thing about that I care about. I have my workflow nearly down to a science, and if I had to change it there would be issues. I run Linux with DWM on two monitors. As mentioned above I'm a Vim user, and I've got several bindings in Vim that make my life easier. I use Vim 8 inside of tmux (more on tmux in a minute). Vim 8 has a builtin terminal which is important to my workflow. A few of my bindings:

1. When FZF is installed: `cnoreabbrev find Files<cr>` and `cnoreabbrev b Buffers<cr>`
2. `tmap <space><space> <C-w>N` binds terminal modes odd escape sequence to my preferred escape sequence.
3. Speaking of escape, I use `<space><space>` as escape. It takes some getting used to since that introduces lag between typing the spacebar and the space appearing on screen, but once you get past that it works beautifully
4. `nnoremap ,, <C-^>` allows me to switch buffers quickly. Taken from Gary Bernhardt.

The first set of bindings for FZF are part of why I don't use EMACS. Given the time I'm sure there's a way to make that work, but as it is I can type `find` at the Vim command line and FZF pops up with no additional input. This is a game changer for me.

The other thing that Vim has as far as workflow that I can get other places, but am used to in Vim is the easy ability to run shell commands over a section of text. For instance:

I can take the following words:

    absences
    absent
    absented
    anaerobic
    anaesthesia
    backstop
    candy
    ubiquity
    ugh
    wags
    yucca
    zoos
    zygote

Highlight them using `vap`, then type `:!sort -R` and get a random order of words, like:

    absences
    absented
    backstop
    zoos
    candy
    anaesthesia
    wags
    yucca
    anaerobic
    ubiquity
    zygote
    absent
    ugh

Then if I want to sort just five words after "zoos" I can do `/zoos<cr> V5j :!sort` like:

    absences
    absented
    backstop
    anaerobic
    anaesthesia
    candy
    wags
    yucca
    zoos
    ubiquity
    zygote
    absent
    ugh

I know these are trivial examples, but this kind of quick access to running shell commands over arbitrary amounts of text is very useful. Also helpful is Vim's new builtin terminal emulator that does a fantastic job of handling every command I throw at it, including running nested Vim instances. (Not recommended, all Vims get very confused.) The most common use for Vim's builtin terminal is monitoring log output.

Enough about Vim. Tmux and DWM are also important parts of my workflow. With tmux I am able to run multiple sessions in a single terminal. This is how I organize my work into projects. I [ have a series of scripts ](https://gitlab.com/yramagicman/stow-dotfiles/tree/master/tmux.d) and a [shell alias](https://gitlab.com/yramagicman/zsh-aliases/blob/master/functions/s) that allow me to spawn new sessions in specific directories. Those familiar with tmux will be aware that of its ability to split terminal windows into multiple windows and probably wonder why I use the built-in Vim terminal in addition to tmux. Sometimes I question that myself, but the benefits of running inside tmux far outweigh the benefits of ditching it.

The other component of my workflow is DWM, or really any customized tiling window manager. I've settled on DWM for now, but I've used I3 and Awesome in the past. The advantage of a tiling window manager, and specifically a dynamic tiling window manager, is that it keeps all your windows arranged in a sane fashion. As a developer, I couldn't live without this feature, and I question those who attempt to. Sure, Windows has it's "aero-snap" or whatever they call it now that allows you to move windows around with keyboard shortcuts, but that's time out of my day that I don't want to lose doing something that my computer should be doing already. (Other Linux desktop environments also have similar window management features, but again, they require work.) In addition to being able to manage windows, DWM provides a couple other features such as:

- Multiple workspaces
-  The ability to assign windows to those specific workspaces, on specific monitors
- follow focus mouse. Whatever window my mouse moves to automatically gains focus.
- Keyboard driven window switching. I can use hot keys to activate different windows without ever touching the mouse.

All of these mean that it (usually) takes less time for me to get an idea from my brain to the screen.

## 3. UNIX (not necessarily UNIX proper, but UNIX-like systems in general)

The last thing for this post that I care about is UNIX. What I do involves processing text, often in as little time as possible. Without tools like grep, sed, awk, and others I would be lost. (I'm aware of grep alternatives and use them regularly, but grep is the most well known search tool, so I chose to mention it.) Most editors that I know of aren't able to handle 6GB+ database dumps, Vim certainly chokes pretty hard on them, but tools like sed handle them easily. This allows me to make "quick edits" to fix import errors when migrating production data to my local environment. I say "quick edits". Really, they still take five or ten minutes to do on my work laptop, but that's due to the size of the files I'm working with, the changes themselves are as simple as replacing one number for another.
