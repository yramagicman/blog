---
title: "Gesture UI is Unintuitive"
date: 2020-01-09T17:57:05-05:00
draft: false
---

## Why are all the buttons gone?

Clearly, this is an opinion, however I have a couple of anecdotes to back this
up, and one that doesn't apply directly but is interesting evidence in favor of
this opinion.

At the risk of sounding like I'm bashing IOS we will begin. My wife and I are
both millennials, and later millennials at that. This is relevant because it is
informative about our background with technology. Both of us are competent with
technology, often in contrast with our boomer generation parents.

## Episode 1

The other night, my wife was attempting to help her dad configure email on his
new iPhone X R. Something wasn't working right so she checked the settings and
confirmed that his servers were configured appropriately. We still aren't sure
what was wrong for two reasons.

1. The error message was entirely unhelpful. (I'll rant about this another time)
2. My wife, who used to be an iPhone user, could not figure out how to bring up
   the app switcher, even with instruction from her parents who I assume have
   figured it out by now.

My wife is a genuinely intelligent woman, and I will freely admit that there are
days when I would be completely lost without her help. She gave up on helping
her dad because the interface to his phone was so opaque that it frustrated her
beyond reason. To highlight this further, during her troubleshooting process she
was instructed to reboot the phone. She was able to shut down his phone after
significant effort both in figuring out how to get to the home screen and
Googling how to turn off the phone. Even once she had the answer it was
difficult because the process involves the rocking both volume buttons and the
lock button rapidly. (I'm recalling this from her frustrated ranting about how
stupid it was that it took 5 button presses to turn off the phone. I almost
certainly have the details wrong.)

## Episode 2

This adventure stars yours truly, and this time I'm at the local cell phone
retailer, again with my wife. We were sorting out an issue with her and adding
her to my plan shortly after we got married almost 2 years ago now, but I
remember clearly experimenting with an iPhone X. I managed to open a few apps
and get back to the home screen with little issue, however when I wanted to
"cover my tracks" and close the apps I had been using I was unable to open the
app switcher intentionally. I accidentally opened it a few times, but my
attempts to close any applications closed the very interface I needed most, the
app switcher. At that point I swore off getting an iPhone for good. I will
always have buttons for basic UI actions. For context, at this point in time I
was employed full time as a developer for a local company, and I couldn't close
an app on a phone.

## Episode 3, the trilogy that was forced

##### ( Quit while you're ahead, when the third episode has nothing to do with the first two, you're doing it wrong. )

This anecdote is here purely to demonstrate how opaque interfaces are only
helpful once you know them. I run [DWM](http://dwm.suckless.org/) on my laptop.
Also, I frequently bring my laptop to my part time job as an attendant at a local
climbing gym so I have something to do when there aren't climbers. On one
occasion I had left my laptop running while some climbers were on the wall. One
of them, a boy no more than 10, saw my computer and said "I'm going to change
your desktop background". This would have been a huge violation of my personal
property, and very rude had he managed actually accomplish his goal. However,
due to the fact that there is no visible interface on my computer when there
aren't applications running, I was unconcerned. I simply replied "you can't",
then let him click around on my computer for 5 minutes before giving up.

My computer is configured to work for me. No matter how smart you are, if there
isn't an application running and you don't know something about how DWM works in
the first place, there's little chance of you stumbling upon a useful function.
I don't have enough key bindings for someone to happen upon one, and the ones I
do have are all for programs that can't do any harm without my password. The
worst you could do is shut down my computer, and that's unlikely to do any harm
since I don't usually run updates from public WI-FI, and I'm always monitoring
the updates.

## The other side of the coin

I used to be a Mac guy. I know that two of my stories were harshing on the new
iOS gesture interface, but that's what I have the most memorable experience
with. When I was running MacOS it was a shining example of intuitive UI. You
can disagree all you want but the fact that a girl no older than 5 was able to
get from my lock screen into the guest account on my Mac while I wasn't looking,
and then get on the internet is proof enough for me that Mac OS is intuitive, or
at least it was 5 years ago. That happened at the aforementioned climbing wall
job in the space of maybe 5 minutes, and I disabled the guest account
immediately after that incident.

Here's an idea. Lets make beautiful interfaces without ignoring the people who
we are forcing these interfaces upon. Apple got rid of the home button at the
cost of intuitive usability. What did we gain by losing the home button? Aside
from a few more inches of screen space, I'm not really sure, and I don't think it
was worth the cost.
